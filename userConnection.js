const { Server } = require("socket.io");
const io = new Server({
  cors: {
    origin: "*"
  }
});
io.listen(8000)
const userConnections = {}

io.on('connection', (socket) => {
    const message = {
      message: 'response global untuk semua user yang connect ke socket'
    }
    socket.emit('connected', JSON.stringify(message))
    userConnections[socket.id] = socket

    socket.on('videotron1:time', (data) => {
      socket.broadcast.emit('videotron1:time', data)
      console.log(data)
    })
    socket.on('videotron2:time', (data) => {
      socket.broadcast.emit('videotron2:time', data)
    })
    socket.on('videotron3:time', (data) => {
      socket.broadcast.emit('videotron3:time', data)
    })

    socket.on('disconnect', function(){
      console.log(userConnections[socket.id].id + ' disconnected');
      delete userConnections[socket.id];
    });
})

module.exports ={userConnections}