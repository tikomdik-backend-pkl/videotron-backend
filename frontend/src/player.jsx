import react, { useEffect, useRef, useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import { io } from "socket.io-client";
import ReactPlayer from "react-player";

function Player() {
  const [message, setMessage] = useState("");
  const [videoUrl, setVideoUrl] = useState(null);
  const [imgurl, setImgUrl] = useState(null);
  const [pptUrl, setpptUrl] = useState(null);
  const [type, setType] = useState();
  const [pause, setPause] = useState(false);
  const socketRef = useRef(null);

  const playerRef = useRef(null);

  useEffect(() => {
    socketRef.current = io("http://10.10.10.34:8000");
    socketRef.current.connect();

    return () => {
      socketRef.current.disconnect();
    };
  }, []);
  useEffect(() => {
    socketRef.current.on("connected", function (data) {
      console.log(data);
    });

    function messageEvent(data) {
      const parsed = JSON.parse(data);
      console.log(parsed);
      let a = parsed.type;
      if (a == "video") {
        setVideoUrl(parsed.data);
      } else if (a == "image") {
        setImgUrl(parsed.data);
      }
      setType(a);
    }
    socketRef.current.on("videotron1", messageEvent);
    socketRef.current.on("videotron1:time", (data) => {
        console.log(data);
        if(data.time) {
          playerRef.current.seekTo(data.time)
          setPause(current => current)
        }
        else{
          setPause(data.paused)
        }
    });
    return () => socketRef.current.off("videotron1", messageEvent);
  }, [socketRef]);
  return (
    <div className="container">
      <ReactPlayer
        ref={playerRef}
        url={`data:video/mp4;base64,${videoUrl}`}
        playing={!pause}
        muted={true}
        controls
      ></ReactPlayer>
    </div>
  );
}

export default Player;
