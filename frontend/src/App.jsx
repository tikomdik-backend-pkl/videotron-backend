import react, { useEffect, useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import { io } from "socket.io-client";

function App() {
  const [data, setData] = useState([]); //useState digunakan untuk menyimpan data atau state lain [] dalam usestate digunakan untuk menandai akan menyimpan array
  const [search, setSearch] = useState(""); // usestate untuk string search
  const [nama, setNama] = useState("");
  const socket = io("ws://localhost:8000");
  const [message, setMessage] = useState("");
  useEffect(() => {
    //pakai use effect untuk menjalankan sebuah function saat page load
    fetch(`http://10.10.10.123:8000/db/`, {
      method: "GET", //secara default jika tidak dimasukkan options akan menggunakan GET
    })
      .then((res) => res.json()) // res mengambil response yang dikirim oleh api, res.json() function untuk mengambil dan format data yang dikirim
      .then((list) => setData(list)); //setData dipanggil untuk menyimpan list kedalam usestate // menggunakan .then callback chain untuk mengkait hasil dari res.json() nama tidak harus list
  }, []); //masukkan array kosong untuk menjalankan useeffect sekali saja

  const filter = data.filter((item) =>
    item.nama.toLowerCase().includes(search.toLowerCase())
  ); // includes merupakan function untuk menjalankan search
  // digunakan tolowercase() agar semua value tida sensitive
  let _csrfToken = null;

  async function getCsrfToken() {
    if (_csrfToken === null) {
      const response = await fetch(`http://10.10.10.123:8000/db/csrf/`, {
        credentials: "include",
      });
      const data = await response.json();
      _csrfToken = data.csrfToken;
    }
    return _csrfToken;
  }

  async function submit() {
    const payload = { nama };
    const csrfToken = await getCsrfToken();
    fetch(`http://10.10.10.123:8000/db/`, {
      method: "POST",
      headers: {
        "X-CSRFToken": csrfToken,
      },
      credentials: "include",
      body: JSON.stringify(payload),
    })
      .then((res) => res.json())
      .then((data) => JSON.parse(data))
      .then((parsed) => alert(parsed.nama));
  }

  useEffect(() => {
    socket.connect();

    socket.on("connected", function (data) {
      console.log(data);
    });
  }, [socket]);

  async function sendMessage() {
    socket.emit("message", message);
    const response = await fetch(
      `http://localhost:4444/public/socket?message=${message}`
    );
  }

  return (
    <div className="container">
      {/* <div className="search-container"> 
        <input type="text" placeholder="Search nama sekolah" onChange={e => setSearch(e.target.value)}/>
        <input type="text" onChange={e => setNama(e.target.value)}/>
        <button onClick={submit}>submit</button>
      </div> */}
      <div className="search-container">
        <input type="text" onChange={(e) => setMessage(e.target.value)} />
        <button onClick={sendMessage}>Send message</button>
      </div>
      {/* <table>
        <thead>
          <tr>
            <th>Nama</th>
            <th>NPSN</th>
          </tr>
        </thead>
        <tbody>
          {
            // buka kurung kurawal untuk menjalankan function di dalam return disini akan digunakan untuk mapping data
            filter.map( //mapping data diganti dengan data filter
              //selain menggunakan function map bisa dengan for loop, lebih mudah pakai map
              (item) => (
                //item akan mengambil data dari array data seperti ref_sekolah_id
                <tr>
                  <td>{item.nama}</td>
                  <td>{item.npsn}</td>
                </tr> // paste html untuk body tabel disini
              )
            ) //  => () sama dengan return ()
          }
        </tbody>
      </table> */}
    </div>
  );
}

export default App;
