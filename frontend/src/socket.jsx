import react, { useEffect, useRef, useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import { io } from "socket.io-client";
import ReactPlayer from 'react-player'

function Socket() {
  const [message, setMessage] = useState("");
  const [videoUrl, setVideoUrl] = useState(null);
  const [imgurl, setImgUrl] = useState(null);
  const [pause, setPause] = useState(false);
  const [type, setType] = useState();
  const [files, setFiles] = useState([]);
  const socketRef = useRef(null);

  useEffect(() => {
    async function fetchFile() {
      const res = await fetch("http://10.10.10.34:4444/public/files", {
        method: "GET",
      });
      const data = await res.json();
      setFiles(data);
      console.log(data);
    }
    fetchFile();
  }, []);
  useEffect(() => {
    socketRef.current = io("http://10.10.10.34:8000");
    socketRef.current.connect();

  return () => {
    socketRef.current.disconnect();
  };
  }, []);
  useEffect(() => {
    socketRef.current.on("connected", function (data) {
      console.log(data);
    });

    function messageEvent(data){
      const parsed = JSON.parse(data);
      console.log(parsed);
      let a = parsed.type;
      if (a == "video") {
        setVideoUrl(parsed.data);
      } else if (a == "image") {
        setImgUrl(parsed.data);
      } 
      setType(a);
    }

    socketRef.current.on("videotron1", messageEvent);
    return () => socketRef.current.off("videotron1", messageEvent);
  },[socketRef])
  async function sendMessage() {
    socketRef.current.emit("message", message);
  }
  function reset() {
    setImgUrl(null);
    setVideoUrl(null);
    setType(null);
  }
  async function selectFile(file){
      const res = await fetch(`http://10.10.10.34:4444/public/socket/${message}?filename=${file}`, {
        method: "GET",
      });
  }
  const playerRef = useRef(null);

  const handlePause = () => {
    setPause(current => !current)
    console.log(pause)
    const payload = {
      paused: pause
    }
    socketRef.current.emit('videotron1:time', payload);

  }

  const handleSeek = (currentTime) => {
    const payload = {
      time: currentTime
    }
    socketRef.current.emit('videotron1:time', payload);
  };

  return (
    <div className="container">
      {type == "image" ? (
        <img src={`data:image;base64,${imgurl}`}></img>
      ) : type == "video" ? (
        <ReactPlayer
        ref={playerRef}
        url={`data:video/mp4;base64,${videoUrl}`}
        controls
        onPause={handlePause}
        onPlay={handlePause}
        muted={true}
        playsinline
        onSeek={handleSeek} 
      ></ReactPlayer>
      ) : null}
      <button onClick={reset}>reset</button>
      <div className="search-container">
        <input type="text" onChange={(e) => setMessage(e.target.value)} />
        <button onClick={sendMessage}>Set user client</button>
      </div>
      <table>
        <thead>
          <tr>
            <th>Nama</th>
            <th>Tipe</th>
            <th>Opsi</th>
          </tr>
        </thead>
        <tbody>
          {files.map((file, i) => (
            <tr key={i}>
              <td>{file.nama_file}</td>
              <td>{file.type}</td>
              <td><button onClick={() => selectFile(file.nama_file)}>Select</button></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default Socket;
