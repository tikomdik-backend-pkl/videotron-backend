import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'
import Socket from './socket'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Player from './player'

ReactDOM.createRoot(document.getElementById('root')).render(
  <BrowserRouter>
  <Routes>
    <Route path="/" element={<Socket/>} />
    <Route path="/player" element={<Player />} />
  </Routes>
</BrowserRouter>
)
