const sequelize = require("../models");
const { userConnections } = require("../userConnection");
const initModels = require("../models/init-models");
const models = initModels(sequelize);
const fs = require("fs/promises");
const mime = require("mime-types");
exports.gFiles = async (req, res) => {
  try {
    const data = await models.files.findAll();
    res.send(data);
  } catch (error) {
    console.error(error);
  }
};

exports.gSocket = async (req, res, next) => {
  try {
    const filename = req.query.filename;
    const videotron = req.params.videotron;
    const data = await models.files.findOne({ where: { nama_file: filename } });
    if (!data) {
      let err = new Error("File not found");
      err.status = 400;
      throw err;
    }
    const videoPath = `files/${data.nama_file}`;
    const buffer = await fs.readFile(videoPath);
    const io = userConnections;
    let base64 = Buffer.from(buffer).toString("base64");
    const message = {
      data: base64,
      type: data.type,
    };
    if (io) {
      for (const key in io) {
        const val = io[key];
        val.emit(videotron, JSON.stringify(message));
        val.emit("connected", JSON.stringify(message));
      }
    }
    res.sendStatus(200);
  } catch (error) {
    console.error(error);
    return res.status(error.status || 500).send(error.message);
  }
};

exports.pSocket = async (req, res) => {
  try {
    if (req.files.length === 0) {
      throw new Error("No file uploaded");
    }
    let ext = mime.extension(req.files[0].mimetype);
    let type = req.files[0].mimetype.slice(
      0,
      req.files[0].mimetype.indexOf("/")
    );
    if (ext === "ppt" || ext === "pptx" || ext === "pdf") type = ext;
    const createpayload = {
      nama_file: req.files[0].originalname,
      type: type,
    };
    const data = await models.files.create(createpayload);
    console.log(createpayload)
    res.send(data);
  } catch (error) {
    console.error(error);
    res.status(error.status || 500).send(error.message);
  }
};

exports.dFiles = async (req, res) => {
  try {
    const id = req.params.id;
    const properties = { where: { file_id: id } };
    console.log(id)

    const found = await models.files.findOne(properties);
    if (!found) {
      let err = new Error("File not found");
      throw err;
    }
    const videoPath = `files/${found.nama_file}`;
    await fs.unlink(videoPath);
    const data = await models.files.destroy(properties);
    if (data) res.send("Successfully deleted");
  } catch (err) {
    console.error(err);
    res.send(err.message);
  }
};
