const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const {userConnections} = require("./userConnection");
const publicRoute = require("./router/publicRoute");
const multer = require('multer')
const fs = require('fs')
const path = require('path');
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'files')
  },
  filename: function (req, file, cb) {
    console.log(file);
    if (fs.existsSync(path.join('files',file.originalname))) {
      const err = new Error('File already exists');
      err.status = 400;
      cb(err);
    }else{
      cb(null, file.originalname)
    }
  }
})

const upload = multer({ storage: storage })
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(upload.any())
app.use('/public', publicRoute)
const PORT = 4444 ;

app.listen(PORT, () => {
  console.log(`listening on port ${PORT}`);
});
