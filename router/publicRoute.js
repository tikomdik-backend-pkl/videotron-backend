const express = require("express");
const { pSocket, gSocket, gFiles, dFiles } = require("../controller/PublicControl");

const router = express.Router();

router.post('/socket', pSocket)
router.get('/socket/:videotron', gSocket)
router.get('/files', gFiles)
router.delete('/files/:id', dFiles)
      
module.exports = router;
