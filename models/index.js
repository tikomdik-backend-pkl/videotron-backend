const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('videotron', 'me', 'password', {
    host: '127.0.0.1',
    port: 5432,
    dialect: 'postgres',
    define: {
        freezeTableName: true
      }
})
module.exports = sequelize