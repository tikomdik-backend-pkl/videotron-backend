const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('files', {
    file_id: {
      type: DataTypes.UUID,
      allowNull: false,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    nama_file: {
      type: DataTypes.STRING,
      allowNull: true
    },
    type: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'files',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "files_pkey",
        unique: true,
        fields: [
          { name: "file_id" },
        ]
      },
    ]
  });
};
